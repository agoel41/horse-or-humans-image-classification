# Horse or Humans image classification

Image classification on the Horse or Humans dataset


**Dataset**

The set contains 500 rendered images of various species of horse in various poses in various locations. It also contains 527 rendered images of humans in various poses and locations. The validation set adds 6 different figures of different gender, race and pose to ensure breadth of data.

- Horses or Humans is a dataset of images of size 300×300 pixels in 24-bit color
- Training set contains 500 horse and 527 human images
- Validation set contains 128 horse and 128 human images
- https://www.kaggle.com/sanikamal/horses-or-humans-dataset
- http://www.laurencemoroney.com/horses-or-humans-dataset/


**Steps to download dataset from Kaggle**

1. search the dataset using the search term 'horses'
	* ```
	kaggle datasets list -s horses
	```

2. List files for the horses-or-humans-dataset
	* ```
	kaggle datasets files sanikamal/horses-or-humans-dataset 
	```

3. Download the dataset to the 'data' folder in the current directory
	* ```
	kaggle datasets download sanikamal/horses-or-humans-dataset -f horse-or-human.zip -p data --unzip
	```

**Libraries**
- tensorflow, matplotlib, numpy



**HorseVsHumansImageClassification-cnn-tf.ipynb:**
The following steps are performed in this notebook

- Download and unzip the dataset
- Display training images to see how the images looks like
- Build CNN Model
- Compile the Model
- Data preprocessing (ImageDataGenerator)
- Train the Model
- Visualize intermediate activation layers